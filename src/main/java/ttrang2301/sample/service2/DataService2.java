package ttrang2301.sample.service2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ttrang2301.sample.interceptor.CheckServiceActivation;

@Service
public class DataService2 {

	public static final String SERVICE_NAME = "data";

	@Value("${contextData:nothing}")
	String data;

	@CheckServiceActivation(serviceName = SERVICE_NAME)
	public String work() {
		System.out.println("I work as Service 2");
		System.out.println(data);
		return SERVICE_NAME;
	}

}
