package ttrang2301.sample.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ttrang2301.sample.condition.ConditionalOnServiceConfiguration;

@ConditionalOnServiceConfiguration(serviceName = DataService.SERVICE_NAME, implementationName = SecondExtendedServiceImplementation.IMPLEMENTATION_NAME)
@Service
public class SecondExtendedServiceImplementation extends DefaultServiceImplementation {

    public static final String IMPLEMENTATION_NAME = "second";

    @Value("${contextData:nothing}")
    String data;

    @Override
    public String work() {
        System.out.println("I work as 2nd Extended Service Implementation");
        System.out.println(data);
        return IMPLEMENTATION_NAME;
    }

}
