package ttrang2301.sample.service;

import ttrang2301.sample.condition.ConditionalOnServiceConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@ConditionalOnServiceConfiguration(serviceName = DataService.SERVICE_NAME, implementationName = DefaultServiceImplementation.IMPLEMENTATION_NAME, isStandardImplementation = true)
@Service
public class DefaultServiceImplementation implements DataService {

    public static final String IMPLEMENTATION_NAME = "default";

    @Value("${contextData:nothing}")
    String data;

    @Override
    public String work() {
        System.out.println("I work as Default Service Implementation");
        System.out.println(data);
        return IMPLEMENTATION_NAME;
    }
}
