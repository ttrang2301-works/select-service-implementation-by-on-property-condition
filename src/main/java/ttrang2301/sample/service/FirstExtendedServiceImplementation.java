package ttrang2301.sample.service;

import ttrang2301.sample.condition.ConditionalOnServiceConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@ConditionalOnServiceConfiguration(serviceName = DataService.SERVICE_NAME, implementationName = FirstExtendedServiceImplementation.IMPLEMENTATION_NAME)
@Service
public class FirstExtendedServiceImplementation extends DefaultServiceImplementation {

    public static final String IMPLEMENTATION_NAME = "first";

    @Value("${contextData:nothing}")
    String data;

    @Override
    public String work() {
        System.out.println("I work as 1st Extended Service Implementation");
        System.out.println(data);
        return IMPLEMENTATION_NAME;
    }

}
