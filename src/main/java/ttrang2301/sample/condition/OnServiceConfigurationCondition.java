package ttrang2301.sample.condition;

import java.util.Map;
import java.util.Objects;

import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.ConfigurationCondition;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.StringUtils;

/**
 * Service implementation can be chosen by property
 * {@code enable-service.<service-name>=<implementation-name>}. If there is no
 * service implementation chosen explicitly by configuration, the standard
 * implementation will be chosen.
 */

public class OnServiceConfigurationCondition implements ConfigurationCondition {

	public static final String PROPERTY_ENABLE_SERVICE = "enable-service.";
	public static final String FIELD_SERVICE_NAME = "serviceName";
	public static final String FIELD_IMPLEMENTATION_NAME = "implementationName";
	public static final String FIELD_IS_STANDARD_IMPLEMENTATION = "isStandardImplementation";

	@Override
	public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {

		Map<String, Object> attributes = annotatedTypeMetadata
				.getAnnotationAttributes(ConditionalOnServiceConfiguration.class.getName());
		Object serviceName = attributes.get(FIELD_SERVICE_NAME);
		Object implementationName = attributes.get(FIELD_IMPLEMENTATION_NAME);
		Boolean isStandardImplementation = (Boolean) attributes.get(FIELD_IS_STANDARD_IMPLEMENTATION);

		String chosenImplementation = conditionContext.getEnvironment()
				.getProperty(PROPERTY_ENABLE_SERVICE + serviceName);

		if (StringUtils.isEmpty(chosenImplementation) && isStandardImplementation) {
			return true;
		}

		if (Objects.equals(chosenImplementation, implementationName)) {
			return true;
		}

		return false;

	}

	@Override
	public ConfigurationPhase getConfigurationPhase() {
		return ConfigurationPhase.REGISTER_BEAN;
	}
}
