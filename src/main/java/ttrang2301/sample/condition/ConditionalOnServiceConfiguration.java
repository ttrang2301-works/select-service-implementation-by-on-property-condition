package ttrang2301.sample.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Application should provide services as interfaces. Each interface could be
 * implemented by numbers of implementations. The implementation chosen at
 * startup time:
 * <ul>
 * <li>Can be configured via property
 * {@code enable-service.<service-name>=<implementation-name>}, or</li>
 * <li>Is standard implementation if there is no configuration</li>
 * </ul>
 * <p>
 * Some restrictions:
 * <ul>
 * <li>There must be only 1 standard implementation, or there would be problem
 * raised by bean container</li>
 * <li>Implementation names must be unique, or there would be problem raised by
 * bean container</li>
 * <li>There should be 1 standard implementation, or there will be problem
 * raised by bean container in case there is no configuration</li>
 * </ul>
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Conditional(OnServiceConfigurationCondition.class)
public @interface ConditionalOnServiceConfiguration {

	/**
	 * @return name of the provided service
	 */
	String serviceName();

	/**
	 * @return name of implementation
	 */
	String implementationName() default "";

	/**
	 * @return true, if this implementation is the default one.
	 */
	boolean isStandardImplementation() default false;

}
