package ttrang2301.sample.interceptor;

import java.lang.reflect.Method;
import java.util.Objects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CheckServiceActivationAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckServiceActivationAspect.class);

	@Autowired
	Environment env;

	@Around("@annotation(CheckServiceActivation)")
	public Object checkServiceActivation(ProceedingJoinPoint joinPoint) throws Throwable {

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		CheckServiceActivation annotation = method.getAnnotation(CheckServiceActivation.class);
		String serviceName = annotation.serviceName();

		if (Objects.isNull(serviceName)) {
			LOGGER.error("Annotation property 'CheckServiceActivation' cannot be null.");
		} else {
			String configuration = env
					.getProperty(CheckServiceActivation.ACTIVATED_SERVICE_CONFIGURATION + serviceName);
			boolean activated = configuration == null ? false : Boolean.parseBoolean(configuration);
			if (!activated) {
				throw new UnsupportedOperationException();
			}
		}

		final Object proceed = joinPoint.proceed();

		return proceed;
	}
}
