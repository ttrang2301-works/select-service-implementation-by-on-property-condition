package ttrang2301.sample.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This interceptor will intercept before the method call at runtime. If the
 * service is not activated, the method will not be called and the caller will
 * catch an {@link UnsupportedOperationException}. Otherwise, the method will be
 * called as normal. The service can be configured via property
 * {@code activated-service.<service-name>=<true/false>}
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckServiceActivation {

	String ACTIVATED_SERVICE_CONFIGURATION = "activated-service.";

	/**
	 * @return name of the provided service
	 */
	String serviceName();

}
