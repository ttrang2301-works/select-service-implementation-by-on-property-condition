package ttrang2301.sample.service2;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import ttrang2301.sample.interceptor.CheckServiceActivation;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = CheckServiceActivation.ACTIVATED_SERVICE_CONFIGURATION + DataService2.SERVICE_NAME
		+ "=" + true)
public class Service2ImplementationActivatedTest {

	@Autowired
	Environment env;

	@Autowired
	DataService2 service;

	@Test
	public void whenServiceActivated_thenServiceReturnsServiceName() {
		assertThat(env.getProperty(CheckServiceActivation.ACTIVATED_SERVICE_CONFIGURATION + DataService2.SERVICE_NAME))
				.isEqualTo(true);
		assertThat(service.work()).isEqualTo(DataService2.SERVICE_NAME);
	}

}
