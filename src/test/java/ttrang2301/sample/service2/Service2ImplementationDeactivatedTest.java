package ttrang2301.sample.service2;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import ttrang2301.sample.interceptor.CheckServiceActivation;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = CheckServiceActivation.ACTIVATED_SERVICE_CONFIGURATION + DataService2.SERVICE_NAME
		+ "=" + false)
public class Service2ImplementationDeactivatedTest {

	@Autowired
	Environment env;

	@Autowired
	DataService2 service;

	@Test
	public void whenServiceDeactivated_thenServiceThrowsUnsupportedOperationException() {
		String configuration = env
				.getProperty(CheckServiceActivation.ACTIVATED_SERVICE_CONFIGURATION + DataService2.SERVICE_NAME);
		String falseValue = false + "";
		assertThat(configuration).isEqualTo(falseValue);
		assertThatThrownBy(service::work).isInstanceOf(UnsupportedOperationException.class);
	}

}
