package ttrang2301.sample.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ttrang2301.sample.condition.OnServiceConfigurationCondition;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + DataService.SERVICE_NAME
		+ "=" + DefaultServiceImplementation.IMPLEMENTATION_NAME)
public class DefaultDataServiceImplementationTest {

	@Autowired
	Environment env;

	@Autowired
	DataService service;

	@Test
	public void whenChosenImplementationIsDefault_thenServiceReturnsDefault() {
		assertThat(env.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + DataService.SERVICE_NAME))
				.isEqualTo(DefaultServiceImplementation.IMPLEMENTATION_NAME);
		assertThat(service.work()).isEqualTo(DefaultServiceImplementation.IMPLEMENTATION_NAME);
	}

}
