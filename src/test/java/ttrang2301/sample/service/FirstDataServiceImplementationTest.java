package ttrang2301.sample.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ttrang2301.sample.condition.OnServiceConfigurationCondition;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + DataService.SERVICE_NAME
		+ "=" + FirstExtendedServiceImplementation.IMPLEMENTATION_NAME)
public class FirstDataServiceImplementationTest {

	@Autowired
	Environment env;

	@Autowired
	DataService service;

	@Test
	public void whenChosenImplementationIsFirst_thenServiceReturnsFirst() {
		assertThat(env.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + DataService.SERVICE_NAME))
				.isEqualTo(FirstExtendedServiceImplementation.IMPLEMENTATION_NAME);
		assertThat(service.work()).isEqualTo(FirstExtendedServiceImplementation.IMPLEMENTATION_NAME);
	}

}
