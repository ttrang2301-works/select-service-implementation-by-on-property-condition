package ttrang2301.sample.condition;

import org.junit.Test;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OnServiceConfigurationConditionTest {

	@Test
	public void whenConfigurationNotFound_andAnnotatedTypeIsStandardImplementation_thenMatchesReturnsTrue() {

		OnServiceConfigurationCondition condition = new OnServiceConfigurationCondition();
		String serviceName = "service-A";

		Environment environment = mock(Environment.class);
		when(environment.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + serviceName))
				.thenReturn(null);
		ConditionContext conditionContext = mock(ConditionContext.class);
		when(conditionContext.getEnvironment()).thenReturn(environment);

		Map<String, Object> attributes = new HashMap<>();
		attributes.put(OnServiceConfigurationCondition.FIELD_SERVICE_NAME, serviceName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IMPLEMENTATION_NAME, "");
		attributes.put(OnServiceConfigurationCondition.FIELD_IS_STANDARD_IMPLEMENTATION, true);
		AnnotatedTypeMetadata annotatedTypeMetadata = mock(AnnotatedTypeMetadata.class);
		when(annotatedTypeMetadata.getAnnotationAttributes(ConditionalOnServiceConfiguration.class.getName()))
				.thenReturn(attributes);

		assertThat(condition.matches(conditionContext, annotatedTypeMetadata)).isEqualTo(true);
	}

	@Test
	public void whenConfigurationNotFound_andAnnotatedTypeIsNotStandardImplementation_thenMatchesReturnsFalse() {

		OnServiceConfigurationCondition condition = new OnServiceConfigurationCondition();
		String serviceName = "service-A";

		Environment environment = mock(Environment.class);
		when(environment.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + serviceName))
				.thenReturn(null);
		ConditionContext conditionContext = mock(ConditionContext.class);
		when(conditionContext.getEnvironment()).thenReturn(environment);

		Map<String, Object> attributes = new HashMap<>();
		attributes.put(OnServiceConfigurationCondition.FIELD_SERVICE_NAME, serviceName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IMPLEMENTATION_NAME, "");
		attributes.put(OnServiceConfigurationCondition.FIELD_IS_STANDARD_IMPLEMENTATION, false);
		AnnotatedTypeMetadata annotatedTypeMetadata = mock(AnnotatedTypeMetadata.class);
		when(annotatedTypeMetadata.getAnnotationAttributes(ConditionalOnServiceConfiguration.class.getName()))
				.thenReturn(attributes);

		assertThat(condition.matches(conditionContext, annotatedTypeMetadata)).isEqualTo(false);
	}

	@Test
	public void whenConfigurationValueEqualsToAnnotatedTypeImplementationName_thenMatchesReturnsTrue() {

		OnServiceConfigurationCondition condition = new OnServiceConfigurationCondition();
		String serviceName = "service-A";
		String implementationName = "service-A-1";

		Environment environment = mock(Environment.class);
		when(environment.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + serviceName))
				.thenReturn(implementationName);
		ConditionContext conditionContext = mock(ConditionContext.class);
		when(conditionContext.getEnvironment()).thenReturn(environment);

		Map<String, Object> attributes = new HashMap<>();
		attributes.put(OnServiceConfigurationCondition.FIELD_SERVICE_NAME, serviceName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IMPLEMENTATION_NAME, implementationName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IS_STANDARD_IMPLEMENTATION, false);
		AnnotatedTypeMetadata annotatedTypeMetadata = mock(AnnotatedTypeMetadata.class);
		when(annotatedTypeMetadata.getAnnotationAttributes(ConditionalOnServiceConfiguration.class.getName()))
				.thenReturn(attributes);

		assertThat(condition.matches(conditionContext, annotatedTypeMetadata)).isEqualTo(true);
	}

	@Test
	public void whenConfigurationValueNotEqualsToAnnotatedTypeImplementationName_thenMatchesReturnsFalse() {

		OnServiceConfigurationCondition condition = new OnServiceConfigurationCondition();
		String serviceName = "service-A";
		String chosenImplementationName = "service-A-1";
		String implementationName = "service-A-2";

		Environment environment = mock(Environment.class);
		when(environment.getProperty(OnServiceConfigurationCondition.PROPERTY_ENABLE_SERVICE + serviceName))
				.thenReturn(chosenImplementationName);
		ConditionContext conditionContext = mock(ConditionContext.class);
		when(conditionContext.getEnvironment()).thenReturn(environment);

		Map<String, Object> attributes = new HashMap<>();
		attributes.put(OnServiceConfigurationCondition.FIELD_SERVICE_NAME, serviceName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IMPLEMENTATION_NAME, implementationName);
		attributes.put(OnServiceConfigurationCondition.FIELD_IS_STANDARD_IMPLEMENTATION, false);
		AnnotatedTypeMetadata annotatedTypeMetadata = mock(AnnotatedTypeMetadata.class);
		when(annotatedTypeMetadata.getAnnotationAttributes(ConditionalOnServiceConfiguration.class.getName()))
				.thenReturn(attributes);

		assertThat(condition.matches(conditionContext, annotatedTypeMetadata)).isEqualTo(false);

	}

}
