#### THIS IS A SAMPLE OF A USER-DEFINED `ConfigrationCondition` OF SPRING FRAMEWORK

In this sample
+ There is 1 interface defining a provided service: `DataService`.
+ There are 3 implementations of `DataService`: `DefaultServiceImplementation`, `FirstExtendedServiceImplementation`, `SecondExtendedServiceImplementation`.

When the application starts, in `ConfigurationPhase.REGISTER_BEAN`, the bean container will choose the implementation bean by Spring property `enable-service.<service-name>=<implementation-name>`.

More details can be found at [JavaDocs](/src/main/java/ttrang2301/sample/condition/ConditionalOnServiceConfiguration.java)

This sample can be tested with:
1. `mvn spring-boot:run`
2. `mvn spring-boot:run -Denable-service.data=first`
3. `mvn spring-boot:run -Denable-service.data=second`